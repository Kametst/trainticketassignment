import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TrainBookingMethods extends PageInit{
WebDriver driver;
    public TrainBookingMethods(WebDriver driver) {
        super(driver);
        this.driver=driver;
    }


    @FindBy(id = "from.search")
    WebElement fromStation;

    @FindBy(id = "to.search")
    WebElement toStation;

    @FindBy(id = "return")
    WebElement ReturnTkt;

    @FindBy(xpath = "//span[contains(text(),'Today')]")
    WebElement leavingDate;

    @FindBy(css = "button[id=onetrust-accept-btn-handler]")
    WebElement acceptCookies;

    @FindBy(xpath = "//span[contains(text(),'Next day')]")
    WebElement returningDate;

    @FindBy(css = "button[data-test='submit-journey-search-button']")
    WebElement searchTickets;



    public void AcceptCookies(){
        acceptCookies.click();
    }
    public void searchFrom(String searchFrom) {
        fromStation.sendKeys(searchFrom);
    }
    public void toStation(String searchTo) {
        toStation.sendKeys(searchTo);
    }
    public void twoWayJourney(){ ReturnTkt.click();}
    public void LeavingDate(){
        leavingDate.click();
    }
    public void ReturningDate(){
        returningDate.click();
    }
    public void searchTicket(){
        searchTickets.click();
    }
}
