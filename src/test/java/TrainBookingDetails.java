import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.concurrent.TimeUnit;


public class TrainBookingDetails {
    public static WebDriver driver;

        @BeforeMethod
        public void setup()
        {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://thetrainline.com");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }


        @Test
        public void Fillup(){
            //Filling Details and Searching
            TrainBookingMethods TrainMethods = new TrainBookingMethods(driver);
            TrainMethods.AcceptCookies();
            TrainMethods.searchFrom("Manchester Piccadilly");
            TrainMethods.toStation("Sheffield");
            TrainMethods.twoWayJourney();
            TrainMethods.LeavingDate();
            TrainMethods.ReturningDate();
            TrainMethods.searchTicket();

            //Getting the Cost
            TicketPrice Cost = new TicketPrice(driver);
            Cost.trainPrice();
            Cost.trainTotalCost();

        }

        @AfterClass
    public void CloseBrowser(){
            driver.quit();
        }

}
