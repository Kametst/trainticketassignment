import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TicketPrice extends PageInit{
    WebDriver driver;

    public TicketPrice (WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    @FindBy(xpath = "//*[@id=\"app\"]/div/div[1]/div[1]/div[1]/div[3]/ul/li[1]/span/span[2]/span[2]/span[2]/span/span")
    WebElement TrainPrice;

    @FindBy(xpath = "//*[@id=\"app\"]/div/div[1]/main/div/div[2]/div/div/div/div/div/div[1]/div[2]/div/div[1]/h3/span[2]/span/span")
    WebElement TotalCost;

    public void trainPrice(){
        System.out.println("Cost of the train :"+TrainPrice.getText());
    }

    public void trainTotalCost(){
        System.out.println("Total Cost : "+TotalCost.getText());
    }

}
